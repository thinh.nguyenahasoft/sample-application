# sample-application

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Note

- Setup sweetalert:
```
npm install vue-sweetalert2
```
- https://codespots.com/library/item/594

### Code Sandbox

```
https://codesandbox.io/s/vue-2-playground-forked-n9y1o5
```

### Old Code

- `CustomerList.vue`

```
<div class="col-12">
        <table
          class="table table-striped table--center"
          v-if="customer_list.length > 0"
        >
          <thead>
            <tr>
              <th scope="row" style="width: 5%">ID</th>
              <th>Name</th>
              <th>Address</th>
              <th>Balance</th>
              <th>Email</th>
              <th>PhoneNumber</th>
              <th>Fax</th>
              <th scope="col">Year Of Birth</th>
              <th scope="col">Actions</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="(customer, index) in customer_list" v-bind:key="index">
              <th scope="row table__cell">{{ customer.id }}</th>
              <td class="text-break table__cell table__cell--width-10">
                {{ customer.name }}
              </td>
              <td class="text-break table__cell table__cell--width-15">
                {{ customer.address }}
              </td>
              <td class="text-break table__cell table__cell--width-10">
                {{ formatMoney(customer.balance) }}
              </td>
              <td class="text-break table__cell table__cell--width-15">
                {{ customer.email }}
              </td>
              <td class="text-break table__cell table__cell--width-10">
                {{ formatPhoneNumber(customer.phoneNumber) }}
              </td>
              <td class="text-break table__cell table__cell--width-10">
                {{ formatFaxNumber(customer.fax) }}
              </td>
              <td class="text-break table__cell table__cell--width-10">
                {{ customer.yearOfBirth }}
              </td>
              <td class="table__cell table__cell--center">
                <!-- <a class="btn btn-success ml-1 mr-1 table__cell__btn">Update</a> -->
                <!-- <a class="btn btn-primary ml-1 mr-1 table__cell__btn">View</a> -->
                <b-button
                  class="ml-1 mr-1 table__cell__btn"
                  variant="success"
                  @click="handleUpdateCustomer(customer.id)"
                  id="show-btn"
                  >Update</b-button
                >
                <b-button
                  id="show-btn"
                  class="btn btn-primary ml-1 mr-1 table__cell__btn"
                  variant="primary"
                  @click="showViewCustomer(customer.id)"
                  >View</b-button
                >
                <a
                  class="btn btn-danger ml-1 mr-1 table__cell__btn"
                  @click.prevent="deleteCustomer(customer.id, customer.name)"
                  >Delete</a
                >
              </td>
            </tr>
          </tbody>
        </table>
        <p class="status-text status-text--center" v-else>{{ status_text }}</p>
      </div>
```