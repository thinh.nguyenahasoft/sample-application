import Vue from 'vue'
import VueSweetalert2 from 'vue-sweetalert2';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import Toaster from 'v-toaster'
import 'v-toaster/dist/v-toaster.css'
import App from './App.vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'sweetalert2/dist/sweetalert2.min.css';
import { BPagination } from 'bootstrap-vue'
Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(VueSweetalert2);
Vue.use(Toaster, {timeout: 5000})
Vue.component('b-pagination', BPagination)

new Vue({
  render: h => h(App),
}).$mount('#app')
